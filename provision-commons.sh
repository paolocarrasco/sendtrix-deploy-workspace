#!/usr/bin/env bash

echo "Provisioning common utilities"

sudo apt-get --yes update

# Essentials
sudo apt-get install --yes build-essential
sudo apt-get install --yes curl
sudo apt-get install --yes wget
sudo apt-get install --yes git
