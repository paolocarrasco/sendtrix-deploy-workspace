# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "ubuntu/trusty32"
  config.vm.hostname = "sendtrix-deploy"
  config.vm.boot_timeout = 500
  config.vm.box_check_update = false
  config.vm.synced_folder "assets", "/home/vagrant/assets", create: true

  # Prevents an issue that arises because Ubuntu's default /root/.profile
  # contains a line that prevents messages from being written to root's console
  # by other users.
  # http://foo-o-rama.com/vagrant--stdin-is-not-a-tty--fix.html
  config.vm.provision "fix-no-tty", type: "shell" do |s|
      s.privileged = false
      s.inline = "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
  end

  config.vm.network "forwarded_port", guest: 3000, host: 8000
  config.vm.network "forwarded_port", guest: 8080, host: 3000

  config.ssh.forward_agent = true

  config.vm.provision "shell", path: "provision-commons.sh"
  config.vm.provision "shell", path: "bootstrap.sh"
  config.vm.provision "shell", path: "clone.sh"
  config.vm.provision "shell", path: "provision-ui.sh"
  config.vm.provision "shell", path: "provision-api.sh"
  config.vm.provision "shell", path: "prepare-launch.sh", run: "always"
  config.vm.provision "shell", path: "launch.sh", run: "always"
end
