#!/usr/bin/env bash

echo "Preparing the launch of Sendtrix"

sendtrixconfig=/home/vagrant/assets/sendtrix-config.properties

if [ ! -d "/var/lock/subsys" ]; then
  mkdir /var/lock/subsys
fi

chmod 700 -R /home/vagrant/sendtrix

chmod +x /home/vagrant/assets/sendtrix-ui-svc /etc/init.d/sendtrix-ui-svc
chmod +x /home/vagrant/assets/sendtrix-api-svc /etc/init.d/sendtrix-api-svc

if [ -e $sendtrixconfig ]; then
  . $sendtrixconfig
  echo "Sendtrix API is being built"
  cd /home/vagrant/sendtrix/sendtrix-api
  ./gradlew build

  echo "Sendtrix UI is being built"
  cd /home/vagrant/sendtrix/sendtrix-ui
  bundle install
  npm install
  bower install --allow-root
  grunt build
  echo "Configuration for Sendtrix was set"
else
  echo "No configuration was found. Check if you have a sendtrix-config.properties file in the assets folder"
  exit 1
fi
