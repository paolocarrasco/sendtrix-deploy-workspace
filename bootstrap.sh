#!/usr/bin/env bash

echo "Bootstraping Sendtrix"

if [ -d "/home/vagrant/sendtrix" ]; then
  rm -rf /home/vagrant/sendtrix
fi
mkdir /home/vagrant/sendtrix

cp /home/vagrant/assets/sendtrix-ui-svc /etc/init.d/sendtrix-ui-svc
cp /home/vagrant/assets/sendtrix-api-svc /etc/init.d/sendtrix-api-svc

touch /home/vagrant/.ssh/config
sudo chown vagrant:vagrant /home/vagrant/.ssh/config
sudo chmod 600 /home/vagrant/.ssh/config

cat << 'EOF' >> /home/vagrant/.ssh/config

StrictHostKeyChecking no

EOF

exit 0
