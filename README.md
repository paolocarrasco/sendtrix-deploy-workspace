# Sendtrix Deploy Workspace
This is a virtual environment for the deployment. It contains both the UI and the API.

## Requirements
- Vagrant
- VirtualBox

## Preparation
1. Clone this repository to a folder in the machine.
```console
$ git clone git@bitbucket.org:paolocarrasco/sendtrix-deploy-workspace.git
```
2. To retrieve the repositories of the application, it is necessary to create the ```bitbucket-credentials.properties``` file with the username and password of the Bitbucket account that has access in the repositories by using the template (```bitbucket-credentials.template.properties```) as reference. Place the file in the same ```assets``` folder and remember to rename it, removing the ```.template``` suffix in the name.
3. To launch the application, we would need to create the ```sendtrix-config.properties``` file with the corresponding values. For that we, use the ```sendtrix-config.template.properties``` file, replace the values and rename the file, removing the ```.template``` suffix in the name.

## Launch
Run the following instructions in the command line in the same folder where the files are stored:

```console
$ vagrant up
```

After that you can check if the application is running in the ports 9000 (UI: http://127.0.0.1:8000) and 8080 (API: http://127.0.0.1:3000).

## Access to the machine
Run the following instructions in the command line in the same folder where the files are stored:

```console
$ vagrant ssh
```
Then you would be able to connect to the machine where is running
