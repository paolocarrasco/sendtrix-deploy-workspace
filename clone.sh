#!/usr/bin/env bash

echo "Cloning the repositories"

credentials=/home/vagrant/assets/bitbucket-credentials.properties

if [ -e $credentials ]; then
  . $credentials
  cd /home/vagrant/sendtrix
  git clone https://$USERNAME:$PASSWORD@bitbucket.org/rquispem/sendtrix-ui.git
  git clone https://$USERNAME:$PASSWORD@bitbucket.org/rquispem/sendtrix-api.git
  echo "Credentials were set"
else
  echo "No credentials for update"
  exit 1
fi
