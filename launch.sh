#!/usr/bin/env bash

echo "Launching the Sendtrix applications"

/etc/init.d/sendtrix-api-svc start
/etc/init.d/sendtrix-ui-svc start

sudo update-rc.d sendtrix-api-svc  defaults
sudo update-rc.d sendtrix-ui-svc  defaults
